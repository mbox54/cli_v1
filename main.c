

#include <stdio.h>
#include <stdlib.h>

#include "cli.h"
#include "cmd_list.h"

int main()
{
    // #Stage 0: intro for user
    printf("Elcub(c) cli program\n");
    printf("version 1.0 \t12.03.2021\n");
    printf("Welcome!\n");
    printf("\n");
    printf("\n");

    // #Stage 1: get command string line
    printf("enter command, please:\n");
	char strCmdLine[64];
	int iStrIndex = 0;

	char c = getchar();
	while ((c != '\n') && (iStrIndex < 64))
    {
        strCmdLine[iStrIndex] = c;
        iStrIndex++;

        c = getchar();
    }

    printf("printed input: %s\n", strCmdLine);

    // test function1
    printf("\nProceed test: \n");
    printf("Cli_ParseCommand(strCmdLine, strEntity, strValue);\n");

    // set buffers
    char strEntity[64];
    char strValue[64];

    Cli_ParseCommand(strCmdLine, strEntity, strValue);

    printf("entity: %s, value: %s\n", strEntity, strValue);
    printf("\n");

    // test function2
    Cli_ExecuteCommand(strEntity, strValue);

    printf("ucVar1 = %d\n", ucVar1);

    // #Stage Exit
    // wait event condition
    printf("\n\nPress any key to exit...");
    getchar();

    return 0;
}
