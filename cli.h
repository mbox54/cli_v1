/**
  ******************************************************************************
  * @file           : cli.h
  * @brief          : Header for cli.c file.
  *                   This file contains prototype functions for cli works
  *
  * core service module type
  ******************************************************************************
  * @attention
  *
  * ver. 1.0
  * ELCUB
  * 16.03.2021
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef CLI_H_INCLUDED
#define CLI_H_INCLUDED

////////////////////////////////////////////////////////////
// Includes
////////////////////////////////////////////////////////////
#include "string.h"


////////////////////////////////////////////////////////////
// Defines
////////////////////////////////////////////////////////////
#define DELIMETER_EXECUTIVE             ' '

typedef enum
{
    CLI_STATUS_STR_VALIDATED,
    CLI_STATUS_STR_RESTRICTED_SYMBOL,
    CLI_STATUS_CMD_FOUND,
    CLI_STATUS_CMD_NOT_FOUND,
} Cli_op_status_t;


////////////////////////////////////////////////////////////
// Function prototypes
////////////////////////////////////////////////////////////

// support
void Cli_ParseCommand(char *strCommand, char *strEntity, char *strValue);
Cli_op_status_t Cli_CheckCommand(char *strEntity, char *strValue);
Cli_op_status_t Cli_ExecuteCommand(char *strEntity, char *strValue);

// main proc
void Cli_proc(char *strCommand);


#endif // CLI_H_INCLUDED
