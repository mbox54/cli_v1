/**
  ******************************************************************************
  * @file           : cli.c
  * @brief          : Implementation for cli.c module.
  *
  * core service module type
  ******************************************************************************
  * @attention
  *
  * ver. 1.0
  * ELCUB
  * 16.03.2021
  *
  ******************************************************************************
  */

////////////////////////////////////////////////////////////
// Includes
////////////////////////////////////////////////////////////
#include "cli.h"
#include "cmd_list.h"


////////////////////////////////////////////////////////////
// Defines
////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////
// Properties
////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////
// Function
////////////////////////////////////////////////////////////



/** DESCRIPTION:
 * Function parse string.
 *
 * @param
 * 'strCommand' valid format:
 * executable [option] <argument>
 * WHERE:
 * 'executable' DEFINITION: is command name.
 * 'executable' SYNTAX: "cmd_line1" (word from letter, no spaces permitted).
 *
 * 'option' DEFINITION: is set of parameters and flags.
 * 'option' SYNTAX: "-x" (short usage has '-' and only one char);
 * "--abcd_efjh" (wide usage has '--' and string without spaces)
 *
 * 'argument' DEFINITION: is value to proceed.
 * 'argument' SYNTAX: " 1234" (value of user type, must spaced before 'executable').
 *
 * @return
 * 'strEntity' is 'executable' part of command.
 * 'strValue' is 'option' + 'argument' part of command.
 *
 * NOTE: 'argument may be another executable with their option and argument'.
 */
// function tries separate 'executable' part from it other part in command string.
void Cli_ParseCommand(char *strCommand, char *strEntity, char *strValue)
{
    int16_t wStrLen = strlen(strCommand);

    // check valid length
    if (wStrLen == 0)
    {
        // [NULL STRING]

        return;
    }

    /** NOTE:
     * - sub string delimiter is: DELIMETER_EXECUTIVE
     * executive can be with value or without (stand alone)
     * stand alone case can be ended without delimeter
     */

    // find end of executive part
    char* pchPosition = strchr(strCommand, DELIMETER_EXECUTIVE);

    // check stand alone executive case
    if (pchPosition == 0)
    {
        // [STAND ALONE]

        // executive is equal input string
        strcpy(strEntity, strCommand);
    }
    else
    {
        // [WITH VALUE]

        uint16_t wExecutiveLen = pchPosition - strCommand;

        // separate executive from other part
        strncpy(strEntity, strCommand, wExecutiveLen);

        // check valid length
        if (wStrLen - wExecutiveLen > 0)
        {
            // [VALID]

            // get value part
            strncpy(strValue, strCommand + wExecutiveLen, wStrLen - wExecutiveLen);
        }
    }//else
}


Cli_op_status_t Cli_CheckCommand(char *strEntity, char *strValue)
{
    // check valid symbols

    return CLI_STATUS_STR_VALIDATED;
}


Cli_op_status_t Cli_ExecuteCommand(char *strEntity, char *strValue)
{
    uint8_t bCmdFound = 0;
    for (uint8_t ucCommandIndex = 0; ucCommandIndex < COMMAND_COUNT; ucCommandIndex++)
    {
        if (strcmp(strEntity, m_aTCommandList[ucCommandIndex].strName) == 0)
        {
            // [COMMAND FOUND]

            // set Flag
            bCmdFound = 1;

            // execute
            m_aTCommandList[ucCommandIndex].ExecuteFunction(strValue);

            // cease finder
            break;
        }
    }

    // check state
    if (!bCmdFound)
    {
        // [INVALID]

        // this command has not found
        return CLI_STATUS_CMD_NOT_FOUND;
    }

    return CLI_STATUS_CMD_FOUND;
}


void Cli_proc(char *strCommand)
{
    // set buffers
    char strEntity[64];
    char strValue[64];

    // parse
    Cli_ParseCommand(strCommand, strEntity, strValue);

    // check
    if (Cli_CheckCommand(strEntity, strValue) != CLI_STATUS_STR_VALIDATED)
    {
        // bad command string

        return;
    }

    // execute
    if (Cli_ExecuteCommand(strEntity, strValue) == CLI_STATUS_CMD_FOUND)
    {
        // executed
    }
    else
    {
        // not found
    }

}
