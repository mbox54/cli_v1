/**
  ******************************************************************************
  * @file           : cmd_list.c
  * @brief          : Storage set of functions for cli.
  *
  * storage module type
  ******************************************************************************
  * @attention
  *
  * ver. 1.0
  * ELCUB
  * 29.03.2021
  *
  ******************************************************************************
  */

////////////////////////////////////////////////////////////
// Includes
////////////////////////////////////////////////////////////
#include "cmd_list.h"


////////////////////////////////////////////////////////////
// Defines
////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////
// Properties
////////////////////////////////////////////////////////////
// test
volatile uint8_t ucVar1 = 0;


// ---------------------------------------------------------
// command variables
// ---------------------------------------------------------
// # help
static const char strCommandName_help[4] = "help";
static const char strCommandDescription_help[21] = "help description";
void ExecuteFunction_help(char * strValue);
// # cmd2
// # cmd3
// # cmd4
// # cmd5
// # ..

// ---------------------------------------------------------
// command list memory
// ---------------------------------------------------------
CommandRecord_t m_aTCommandList[COMMAND_COUNT] =
{
    // # help
    {
        .strName = strCommandName_help,
        .strDescription = strCommandDescription_help,
        .ExecuteFunction = ExecuteFunction_help,
    },
    //
    {
        .strName = strCommandName_help,
        .strDescription = strCommandDescription_help,
        .ExecuteFunction = ExecuteFunction_help,
    }
};


////////////////////////////////////////////////////////////
// Function
////////////////////////////////////////////////////////////
void ExecuteFunction_help(char * strValue)
{
    ucVar1 = 1;
}


