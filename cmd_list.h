/**
  ******************************************************************************
  * @file           : cli.h
  * @brief          : Header for cli.c file.
  *                   Storage set of functions for cli.
  *
  * storage module type
  ******************************************************************************
  * @attention
  *
  * ver. 1.0
  * ELCUB
  * 29.03.2021
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef CMD_LIST_H_INCLUDED
#define CMD_LIST_H_INCLUDED


////////////////////////////////////////////////////////////
// Includes
////////////////////////////////////////////////////////////
#include "stdint.h"


////////////////////////////////////////////////////////////
// Defines
////////////////////////////////////////////////////////////
// main
typedef struct
{
    const char * strName;
    const char * strDescription;
    void (*ExecuteFunction)(char * strValue);
} CommandRecord_t;


#define COMMAND_COUNT               2


////////////////////////////////////////////////////////////
// Properties
////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////
// Function prototypes
////////////////////////////////////////////////////////////
// externs
extern CommandRecord_t m_aTCommandList[COMMAND_COUNT];
// test
extern volatile uint8_t ucVar1;

// command list
void ExecuteFunction_help(char * strValue);


#endif // CMD_LIST_H_INCLUDED

